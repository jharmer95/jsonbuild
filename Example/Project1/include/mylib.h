#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

double FindHypotenuse(double a, double b);

#ifdef __cplusplus
}
#endif
