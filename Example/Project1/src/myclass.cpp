#include "myclass.hpp"

MyClass::MyClass() {
  number = 0;
  name = "Default";
}

MyClass::MyClass(int n, std::string s) : number(n), name(s) {}

int MyClass::GetNumber() { return number; }

std::string MyClass::GetName() { return name; }

void MyClass::SetName(const std::string &s) { name = s; }
