#include "defs.hpp"
#include "myclass.hpp"
#include "mylib.h"
#include <iostream>

int main()
{
  std::cout << "Version: " << MAJOR_VERSION << "." << MINOR_VERSION << std::endl
            << std::endl;

  MyClass mc(4, "Harold");

  std::cout << "Name: " << mc.GetName() << ", Number: " << mc.GetNumber()
            << std::endl;

  double leg1 = 4.50;
  double leg2 = 3.55;

  double hyp = FindHypotenuse(leg1, leg2);

  std::cout << "Hypotenuse is " << hyp << std::endl;

  return 0;
}
