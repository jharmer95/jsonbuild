#include <iostream>
#include "magicnumber.hpp"

int main()
{
    std::cout << "Hello, world!" << std::endl;

    std::cout << "Magic number: " << MAGIC_NUMBER << std::endl;

    return 0;
}